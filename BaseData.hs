module BaseData where

import Types
import Tools
import qualified Data.Map as M



trainNetwork :: TrainNetwork
trainNetwork = [(("b", "a"), [1]), 
        (("b", "c"), [2]), 
        (("b", "d"), [1,2]), 
        (("d", "e"), [2]), 
        (("f", "d"), [3]), 
        (("f", "c"), [3]), 
        (("e", "g"), [2])
        ]



-- Anzahl der Sitze pro Zug, die nicht reserviert werden koennen
freeSeatsMap :: M.Map (LineID, TrainID) Integer
freeSeatsMap = M.fromList [((1,1), 3), 
                           ((1,2), 1), 
                           ((2,1), 1), 
                           ((3,1), 0), 
                           ((3,2), 0)]

unbookable :: LineID -> TrainID -> TrainData -> Integer
unbookable lid tid trainData = let train = M.lookup lid trainData >>= M.lookup tid
                               in case train of
             	                       Nothing  -> 0
             	                       _        -> justLookup (lid,tid) freeSeatsMap
             	     

linien :: TrainData
linien = M.fromList [(1,linie1), (2,linie2), (3,linie3)]

linie1 :: Line
linie1 = M.fromList [(1,l1t1), (2,l1t2)]

linie2 :: Line
linie2 = M.fromList [(1,l2t1)]

l1t1 :: Train
l1t1 = M.fromList [(1,l1t1w1), (2,l1t1w2)]

l1t2 :: Train
l1t2 = M.fromList [(1,l1t2w1)]


l2t1 :: Train
l2t1 = M.fromList [(1,l2t1w1), (2,l2t1w2)]

emptyWagon :: Integer -> Wagon
emptyWagon n = M.fromList $ zip [1..n] (repeat [])

l1t1w1 = emptyWagon 3
l1t1w2 = emptyWagon 4
l1t2w1 = emptyWagon 2
l2t1w1 = emptyWagon 3
l2t1w2 = emptyWagon 2
l3t1w1 = emptyWagon 1
l3t2w1 = emptyWagon 2
l3t2w2 = emptyWagon 3

linie3 :: Line
linie3 = M.fromList [(1, l3t1), (2, l3t2)]

l3t1 :: Train
l3t1 = M.fromList [(1, l3t1w1)]

l3t2 :: Train
l3t2 = M.fromList [(1, l3t2w1), (2,  l3t2w2)]






-- Simplere Testdaten
--trainNetwork = [(("a","b"), [1]), (("b","c"),[1])]
--t1 :: Train
--t1 = M.fromList [(1,w1),(2,w2)]
--w1 = M.fromList [(1,[("a","b")]), (2,[("a","b")]), (3, [])]
--w2 = M.fromList [(1,[]), (2,[]), (3,[]), (4, [])]

--t2 :: Train
--t2 = M.fromList [(1,w2),(2,w2), (3,w2)]

--l1 :: Line
--l1 = M.fromList [(1,t1), (2,t2)]

--lin = M.fromList [(1,l1)]