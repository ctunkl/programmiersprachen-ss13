module Logic where

import Data.List 
import qualified Data.Map as M
import Data.Maybe

import Types
import Tools
import BaseData


-- Hauptfunktionen. Rufen die tatsaechlich rechenden Funktionen auf und liefern 
-- ein Tupel mit den neuen Daten und der Nachricht fuer den User

reserve :: LineID -> Integer -> [RouteSection] -> TrainData -> (TrainData, String)
reserve lid seats sections trainData
  | null options = (trainData, "Not enough seats")
  | otherwise = book (head options) sections trainData
  where options = bookingOptions lid seats sections trainData      


minFree :: LineID -> TrainID -> WagonID ->  [RouteSection] -> TrainData -> (TrainData, String)
minFree lid tid wagonID sections trainData = let line = justLookup lid trainData
                                                 train = justLookup tid line
                                                 result = minFree_Route train (unbookable lid tid trainData) wagonID sections
                                                 resultMessage = (show result) ++ " seats will be free for sure."
                                             in (trainData, resultMessage)
  
-- todo: soll auch fuer mehrere zuege gehen
minFree_Train :: LineID -> TrainID -> [RouteSection] -> TrainData -> (TrainData, String)
minFree_Train lid tid sections trainData = let line = justLookup lid trainData
                                               train = justLookup tid line
                                               result = minimum $ map (freeSeatsCountForSection_Train train) sections
                                               resultMessage = (show result) ++ " seats will be free for sure."
                                           in (trainData, resultMessage)


maxBooked :: LineID -> TrainID -> WagonID -> [RouteSection] -> TrainData -> (TrainData, String)
maxBooked lid tid wid sections trainData = let line = justLookup lid trainData
                                               train = justLookup tid line
                                               result = calculateMaxBooked train (unbookable lid tid trainData) wid sections
                                               resultMessage = "Up to " ++ (show result) ++ " seats are booked."
                                         in (trainData, resultMessage)

-- todo: soll auch fuer mehrere zuege gehen
minBookable :: LineID -> TrainID -> [RouteSection] -> TrainData -> (TrainData, String)
minBookable lid tid sections trainData = let line = justLookup lid trainData
                                             train = justLookup tid line
                                             result = calculateMinBookable train (unbookable lid tid trainData) sections
                                             resultMessage = "Up to " ++ (show result) ++ " seats can be booked (not necessarily at once)."
                                         in (trainData, resultMessage)


-- todo: soll auch fuer mehrere zuege gehen
maxGroupSize :: LineID -> TrainID -> [RouteSection] -> TrainData -> (TrainData, String)
maxGroupSize lid tid sections trainData = let line = justLookup lid trainData
                                              train = justLookup tid line
                                              result = calculateMaxGroupSize lid tid sections trainData
                                              resultMessage = "You can book up to " ++ (show result) ++ " seats at once."
                                           in (trainData, resultMessage)


-----
-----


lookupSeat :: LineID -> TrainID -> WagonID -> SeatNumber -> TrainData -> (TrainData, String)
lookupSeat lid tid wid seat trainData = let result = M.lookup lid trainData >>= M.lookup tid >>= M.lookup wid >>= M.lookup seat
                                        in case result of
                                                Nothing -> (trainData, "Invalid input data")
                                                Just seats -> (trainData, show $ seats)


calculateMaxGroupSize :: LineID -> TrainID -> [RouteSection] -> TrainData -> Integer
calculateMaxGroupSize lid tid sections trainData = let options = map (\i -> (i,bookingOptions lid i sections trainData)) [1..]
                                                       filterByTrain = filter (\(_,t,_,_) -> t == tid)
                                                       optionsForTrain = map (\(i, opt) -> (i, filterByTrain opt)) options
                                                       validOptions = takeWhile (not.null.snd) optionsForTrain
                                                   in  case validOptions of
                                                            [] -> 0
                                                            xs -> fst $ last xs


minFree_Route :: Train -> Integer -> WagonID -> [RouteSection] -> Integer
minFree_Route train freeSeats wagonID sections = let wagon = justLookup wagonID train
                                                 in minimum $ map (freeSeatsCountForSection_Wagon wagon) sections


minBookable_Section :: Train -> Integer -> RouteSection -> Integer
minBookable_Section train freeSeats sec = let freeSeatsCount = sum $ map (\wag -> freeSeatsCountForSection_Wagon wag sec) (M.elems train)                                                     
                                          in freeSeatsCount - freeSeats

calculateMinBookable :: Train -> Integer -> [RouteSection] -> Integer
calculateMinBookable train freeSeats sections = minimum $ map (minBookable_Section train freeSeats) sections


calculateMaxBooked :: Train -> Integer -> WagonID -> [RouteSection] -> Integer
calculateMaxBooked train freeSeats wagonID sections = maximum $ map (numberOfBookedSeats (justLookup wagonID train)) sections


numberOfBookedSeats :: Wagon -> RouteSection -> Integer
numberOfBookedSeats wag sec = len $ filter (\sections -> elem (sortSection sec) sections) (M.elems wag)

minFreeSeatsPerWagon :: Train -> Integer -> WagonID -> RouteSection -> Integer
minFreeSeatsPerWagon train freeSeats wagonID sec = foldr (\wag acc -> f wag acc) freeSeats (M.elems train')
  where train' = M.delete wagonID train
        f _ 0 = 0
        f wag fs
          | free > fs = 0
          | otherwise = (fs-free)
          where free = freeSeatsCountForSection_Wagon wag sec


-- Liefert alle Wagons, in denen noch genug Plaetze fuer alle Streckenabschnitte sind, und die
-- entsprechenden Sitznummern
bookingOptions :: LineID -> Integer -> [RouteSection] -> TrainData -> [(LineID, TrainID, WagonID, SeatRange)]
bookingOptions lid seats sections trainData = [(lineID, trainID, wagonID, firstFreeSeatRangeOfLength seats wagon sections) 
                                                     | (lineID, line) <- M.assocs trainData, 
                                                       (trainID, train) <- M.assocs line,
                                                       (wagonID, wagon) <- M.assocs train,                                           
                                                       lineID == lid,
                                                       passengersWithinLimit train (unbookable lid trainID trainData) sections seats,
                                                       (not.null ) $ firstFreeSeatRangeOfLength seats wagon sections]


-- Sind fuer jede Section genug Plaetze frei?
passengersWithinLimit :: Train -> Integer -> [RouteSection] -> Integer -> Bool
passengersWithinLimit _ _ [] _ = True
passengersWithinLimit train freeSeats (sec:sections) seatsToBook 
  | (totalFreeSeats-seatsToBook) < freeSeats = False
  | otherwise = passengersWithinLimit train freeSeats sections seatsToBook
  where totalFreeSeats = freeSeatsCountForSection_Train train sec




-- Fuehrt die Buchung durch. Geht bis zum untersten Dict, haengt die neuen Streckenabschnitte an und aktualisiert
-- dann von unten nach oben alle Dicts
book :: (LineID, TrainID, WagonID, SeatRange) -> [RouteSection] -> TrainData -> (TrainData, String)
book (lineID, trainID, wagonID, seatsToBook) sections trainData = let line = justLookup lineID trainData
                                                                      train = justLookup trainID line
                                                                      wagon = justLookup wagonID train
                                                                      sortedSections = map sortSection sections
                                                                      wagon' = foldl (\m (seatNumber,sec) -> M.insert seatNumber (sec ++ sortedSections) m) wagon seatsToBook
                                                                      train' = M.insert wagonID wagon' train
                                                                      line' = M.insert trainID train' line
                                                                      seats = map fst seatsToBook
                                                                      message = "Confirming reservation: Line " ++ show lineID ++ ", Train " ++ show trainID ++ ", Wagon " ++ show wagonID ++ ", Seats " ++ show seats
                                                                  in (M.insert lineID line' trainData, message)




-- Liefert jene Sitze, auf denen fuer einer Strecke noch Platz ist. Auf einmal gebuchte Sitze muessen
-- zusammenhaengen, deswegen alle Sub-Listen der korrekten Laenge suchen und jene herausfiltern, in denen kein Sitz belegt auf
-- den Streckenabschnitten belegt ist
freeSeatRanges :: Wagon -> [RouteSection] -> [SeatRange]
freeSeatRanges wagon sections = let subRanges = sublists $ M.assocs wagon
                                in filter (\sr -> all (\(_, sec) -> null $ intersect sec sections) sr) subRanges
                                    

firstFreeSeatRange :: Wagon -> [RouteSection] -> SeatRange 
firstFreeSeatRange wagon sections = emptyHead $ freeSeatRanges wagon sections

firstFreeSeatRangeOfLength :: Integer -> Wagon -> [RouteSection] -> SeatRange
firstFreeSeatRangeOfLength seats wagon sections = emptyHead $ seatRangesOfLength seats $ freeSeatRanges wagon sections

seatRangesOfLength :: Integer -> [SeatRange] -> [SeatRange]
seatRangesOfLength seats = filter ((==seats) . len)


routeForLine :: LineID -> [Station]
routeForLine lineID = let sections = map fst $ filter (\(seg, trainLines) -> elem lineID trainLines) trainNetwork
                      in createRoute sections


-- Findet eine Teilstrecke einer Linie zwischen zwei Stationen. Muss normal und reversed gemacht werden,
-- weil sliceWithElements x y davon ausgeht, dass x vor y kommt.
partialRouteSectionsForLine :: LineID -> Station -> Station -> [RouteSection]
partialRouteSectionsForLine lineID source destination = let route = routeForLine lineID
                                                            normal = sliceWithElements source destination route
                                                            rev = sliceWithElements destination source route
                                                            final = if null normal then rev else normal
                                                        in map sortSection $ zip final (tail final)


-- Findet Endstationen, erzeugt Gesamtstrecke
createRoute sections = let (as,bs) = unzip sections
                           [firstStation, lastStation] = findUniqueElements (as ++ bs)                      
                       in concatRoute firstStation lastStation sections




sortSection :: RouteSection -> RouteSection
sortSection (x,y)
  | x > y = (y,x)
  | otherwise = (x,y)

findUniqueElements :: Ord a => [a] -> [a]
findUniqueElements = concat . filter singleton . group . sort

-- Endstation + Endstation + alle Streckenabschnitte --> Strecke, als Liste von Stationen. Funktioniert nicht mit Kreisen!
concatRoute firstStation lastStation sections = cRoute lastStation sections [firstStation]

cRoute lastStation sections route 
  | elem (last route, lastStation) sections || elem (lastStation, last route) sections = route ++ [lastStation]
  | otherwise = cRoute lastStation (delete tuple sections) (route ++ [nextDestination])
  where tuple@(nd1,nd2) = head $ filter (\(s,d) -> s == (last route) || d == (last route)) sections
        nextDestination = if (last route) == nd1 then nd2 else nd1


freeSeatsCountForSection_Wagon :: Wagon -> RouteSection -> Integer
freeSeatsCountForSection_Wagon wag sec = len $ filter (not . elem sec) (M.elems wag)

freeSeatsCountForSection_Train :: Train -> RouteSection -> Integer
freeSeatsCountForSection_Train train sec = sum $ map (\wag -> freeSeatsCountForSection_Wagon wag sec) (M.elems train)

