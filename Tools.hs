module Tools where
import Data.List
import Data.Maybe
import qualified Data.Map as M


len :: Num b => [a] -> b
len = genericLength

printLn :: Show a => a -> IO ()
printLn = putStrLn . show

singleton :: [a] -> Bool
singleton [x] = True
singleton _ = False


slice :: Int -> Int -> [a] -> [a]
slice begin end = take (end - begin + 1) . drop (begin)

-- Returned die Subliste zwischen zwei Elementen (inklusive), oder ne leere Liste wenn nicht
-- beide Elemente vorhanden sind
sliceWithElements :: Eq a => a -> a -> [a] -> [a]
sliceWithElements start end xs = let si = findIndex (==start) xs
                                     ei = findIndex (==end) xs
                                 in if (si == Nothing || ei == Nothing) 
                                 	then []
                                 	else 
                                 		slice (fromJust si) (fromJust ei) xs


-- don't make me extract Maybes, justLookup! smart, right? right??
justLookup :: Ord k => k -> M.Map k a -> a
justLookup k m = fromJust $ M.lookup k m

maybeHead :: [a] -> Maybe a
maybeHead [] = Nothing
maybeHead xs = Just $ head xs

sublists :: Eq a => [a] -> [[a]]
sublists xs = nub $ filter (not.null) $ concatMap tails $ inits xs

emptyHead :: [[a]] -> [a]
emptyHead [] = []
emptyHead xs = head xs