import Data.Maybe
import Data.List
import qualified Data.Map as M

import BaseData
import Types
import Tools
import Logic



{- Die Hauptschleife bekommt initial die Defaultdaten (linien) und fragt dann nach dem Benutzerinput. Das wird 
dann an dispatch weitergegeben, das per Pattern Matching eine entsprechende IO action returned. 
Diese muss wiederum ein TrainData-Object returnen. Viel gibt's zum Code nicht zu erklaeren. 
Das raw-Command ist zum Debuggen nuetzlich, es gibt einfach das TrainData-Objekt aus ("view trains" formatiert den Output)
-}



main :: IO ()
main = do 
         putStrLn ""
         putStrLn "Welcome to <FunnyTrainAppNameHere>. Enter 'help' to see a list of commands."
         mainLoop linien


mainLoop :: TrainData -> IO()
mainLoop oldData = do  
  putStrLn ""
  putStr "> "
  line <- getLine
  if line == "exit" 
    then return ()
    else do 
           newData <- dispatch (words line) oldData  
           mainLoop newData

        


dispatch ["save", fileName] trainData = do 
                                        writeFile fileName (show trainData)
                                        putStrLn ("Saved data to " ++ fileName)
                                        return trainData

dispatch ["load", fileName] trainData = do
                                        fileContent <- readFile fileName
                                        let loadedData = read fileContent
                                        putStrLn ("Loaded data from " ++ fileName)
                                        return loadedData

dispatch ["view", "trains"] trainData = do 
                                        viewTrains trainData
                                        return trainData

dispatch ["view", "stations"] trainData = do 
                                          viewStations trainNetwork
                                          return trainData

 

dispatch ["raw"] trainData = do
                             putStrLn $ show trainData
                             return trainData


dispatch ["help"] trainData = do
                              putStrLn "view [trains|stations]:\t\t  Displays an overview of all trains or stations"
                              putStrLn "view line train wagon seatID:\t  Displays all route sections for which the specified seat has been booked"                              
                              putStrLn "reserve line n A B:\t\t  Attempts to reserve [n] seats on line [lineID] between the two specified stations"
                              putStrLn "minFree line train [wagon] A B:\t  Tells you how many seats in a wagon/train will be guaranteed to be free between two stations"
                              putStrLn "minBookable line train wagon A B: Tells you how many seats in a wagon will possible to book at least"
                              putStrLn "maxBooked line train wagon A B: Tells you how many seats in a wagon will be occupied at most"
                              putStrLn "maxGroupSize line train A B:\t  Tells you how many seats in a wagon will possible to book at once"
                              putStrLn "save fileName:\t\t\t  Saves the data to the specified file"
                              putStrLn "load fileName:\t\t\t  Loads the data from the specified file into the program"
                              putStrLn "help:\t\t\t\t  Take a guess."                              
                              putStrLn "exit:\t\t\t\t  Exit program and save data"
                              return trainData



dispatch ["minFree", lineID, trainID, wagonID, source, destination] trainData = do
                                                                                let sections = partialRouteSectionsForLine (read lineID) source destination 
                                                                                let result = minFree (read lineID) (read trainID) (read wagonID) sections trainData
                                                                                evaluateInput_ReturnResult True lineID sections trainData result

dispatch ["minFree", lineID, trainID, source, destination] trainData = do
                                                                       let sections = partialRouteSectionsForLine (read lineID) source destination 
                                                                       let result = minFree_Train (read lineID) (read trainID) sections trainData
                                                                       evaluateInput_ReturnResult True lineID sections trainData result

dispatch ["maxBooked", lineID, trainID, wagonID, source, destination] trainData = do
                                                                                  let sections = partialRouteSectionsForLine (read lineID) source destination 
                                                                                  let result = maxBooked (read lineID) (read trainID) (read wagonID) sections trainData
                                                                                  evaluateInput_ReturnResult True lineID sections trainData result


dispatch ["maxGroupSize", lineID, trainID, source, destination] trainData = do
                                                                            let sections = partialRouteSectionsForLine (read lineID) source destination 
                                                                            let result = maxGroupSize (read lineID) (read trainID) sections trainData
                                                                            evaluateInput_ReturnResult True lineID sections trainData result




dispatch ["minBookable", lineID, trainID, source, destination] trainData = do
                                                                           let sections = partialRouteSectionsForLine (read lineID) source destination 
                                                                           let result = minBookable (read lineID) (read trainID) sections trainData
                                                                           evaluateInput_ReturnResult True lineID sections trainData result



dispatch ["view", lineID, trainID, wagonID, seatNumber] trainData = do
                                                                    let result = lookupSeat (read lineID) (read trainID) (read wagonID) (read seatNumber) trainData
                                                                    evaluateInput_ReturnResult False lineID [] trainData result


dispatch ["reserve", lineID, seats, source, destination] trainData = do
                                                                     let sections = partialRouteSectionsForLine (read lineID) source destination 
                                                                     let result = reserve (read lineID) (read seats) sections trainData
                                                                     evaluateInput_ReturnResult True lineID sections trainData result




dispatch _ trainData = do 
                       putStrLn "Unknown command"
                       return trainData


evaluateInput_ReturnResult evaluateSections lineID sections trainData (newData, result) = do
                                                         let (isValidInput, errorMessage) =  validInput evaluateSections (read lineID) sections trainData
                                                         if not isValidInput
                                                           then do
                                                                putStrLn errorMessage
                                                                return trainData
                                                           else do
                                                                putStrLn result
                                                                return newData


validInput :: Bool -> LineID -> [RouteSection] -> TrainData -> (Bool, String)
validInput evaluateSections lid sections trainData
  | not $ M.member lid trainData = (False, "Line does not exist")  
  | evaluateSections && null sections = (False, "Line does not travel between those two stations")
  | otherwise = (True, "")


viewStations :: TrainNetwork -> IO [()]
viewStations sections = mapM viewStation (groupStations $ map fst sections)

viewStation :: (Station, [Station]) -> IO ()
viewStation (s, neighbours) = putStrLn $ "Station " ++ s ++ " is connected to: " ++ (concat $ intersperse ", " neighbours)

groupStations :: [RouteSection] -> [(Station, [Station])]
groupStations sections = let stations = nub $ sort $ (map fst sections) ++ (map snd sections)
                         in map (\s -> (s, connectedStations sections s)) stations


connectedStations sections s =  let s1 = map snd $ filter ((==s).fst) sections
                                    s2 = map fst $ filter ((==s).snd) sections
                                in nub $ sort $ (s1 ++ s2)


viewTrains :: TrainData -> IO [[[()]]]
viewTrains trainData = mapM (viewLine trainData) (M.assocs trainData)
                       

viewLine :: TrainData -> (LineID, Line) -> IO [[()]]
viewLine trainData (lineID, line) = do
                            putStrLn $ "Line " ++ show lineID ++ " (Route: " ++ (concat $ intersperse ", " $ routeForLine lineID) ++ ")"
                            mapM (viewTrain trainData lineID) (M.assocs line)

viewTrain :: TrainData -> LineID -> (TrainID, Train) -> IO [()]
viewTrain trainData lid (trainID, train) = do                               
                               putStrLn ("  Train " ++ show trainID ++ " (" ++ show (unbookable lid trainID trainData) ++ " seats unbookable)")
                               mapM viewWagon (M.assocs train)


viewWagon :: (WagonID, Wagon) -> IO ()
viewWagon (wagonID, wagon) = putStrLn ("    Wagon " ++ show wagonID ++ " (capacity = " ++ (show $ M.size wagon) ++ ")")

