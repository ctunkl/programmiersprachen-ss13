module Types where
import qualified Data.Map as M


type TrainData = M.Map LineID Line 

type ID = Integer
type TrainID = ID
type WagonID = ID
type LineID = ID

type SeatNumber = Integer

type SeatRange = [(SeatNumber, [RouteSection])]

type Line = M.Map TrainID Train
type Train = M.Map WagonID Wagon
type Wagon = M.Map SeatNumber [RouteSection]


type TrainNetwork = [(RouteSection, [TrainID])]
type RouteSection = (Station, Station)
type Station = String
